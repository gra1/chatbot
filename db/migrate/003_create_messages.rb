require 'active_record'

class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text :body
      t.integer :conversation_id
      t.integer :user_id
      t.timestamp :created_at
    end
  end
end
