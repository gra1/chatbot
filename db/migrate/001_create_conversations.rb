require 'active_record'

class CreateConversations < ActiveRecord::Migration
  def change
    create_table :conversations do |t|
      t.timestamp :created_at
    end
  end
end
