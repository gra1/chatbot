require 'active_record'
require 'yaml'

dbconfig = YAML::load_file('config/database.yml')

ActiveRecord::Base.establish_connection(dbconfig)
