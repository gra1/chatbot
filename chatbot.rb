require File.expand_path('../config/environment', __FILE__)
require_relative 'app/chatbot_facade.rb'
require_relative 'app/bot_message_store.rb'

ChatbotFacade.new(BotMessageStore::MESSAGES,
                  BotMessageStore::PRIMARY_TYPE).perform
