require File.expand_path('../config/environment', __FILE__)
require 'rake/testtask'

namespace :db do
  desc "Migrate the database"
  task :migrate do
    ActiveRecord::Migrator.migrate('db/migrate')
  end

  desc 'Rollback the database'
  task :rollback do
    ActiveRecord::Migrator.rollback('db/migrate')
  end
end

desc 'Run test'
task :test do
  Rake::TestTask.new do |task|
    task.libs << %w(test)
    task.pattern = 'test/*_test.rb'
  end
end
