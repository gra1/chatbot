require 'minitest/autorun'
require 'pry'
require_relative '../config/environment.rb'
require_relative '../app/chatbot_facade.rb'

def chatbot_facade
  @_cahtbot_facade ||= ChatbotFacade.new(bot_messages, :welcome)
end

def change_current_type_to(type)
  chatbot_facade.current_type = type
end

def add_user_message(message)
  chatbot_facade.user_message = message
end

def change_current_bot_data
  chatbot_facade.current_bot_data = chatbot_facade.find_current_bot_data
end

def bot_messages
  @_bot_messages ||= [
    {
      message: 'Welcome message',
      aditional_message: 'Aditional message',
      type: :welcome,
      obligatory_next_type: :name
    },
    {
      message: 'Please enter your name',
      type: :name
    },
    {
      message: 'Hello NAME',
      type: :reach_out
    },
    {
      message: 'Approve CONTACT_VALUE',
      type: :contact_approve,
      relative_next_type: [
        {'1' => :welcome},
        {'2' => :name}
      ]
    }
  ]
end
