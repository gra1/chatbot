require 'test_helper'

class BotMessageBuilderTest < Minitest::Test
  def test_build_bot_message
    assert_equal 'Welcome messageAditional message', chatbot_facade.build_bot_message
  end

  def test_prepared_bot_message_with_reach_out_type
    change_current_type_to :reach_out
    change_current_bot_data
    add_user_message 'Wan'

    assert_equal 'Hello Wan', chatbot_facade.prepared_bot_message
  end

  def test_prepared_bot_message_with_contact_approve_type
    change_current_type_to :contact_approve
    change_current_bot_data
    add_contact_value

    assert_equal 'Approve 555', chatbot_facade.prepared_bot_message
  end

  def test_prepared_bot_message_with_welcome_type
    assert_equal 'Welcome message', chatbot_facade.prepared_bot_message
  end

  def test_find_current_bot_data
    assert_equal bot_messages.first, chatbot_facade.find_current_bot_data
  end

  def test_additional_bot_message
    assert_equal 'Aditional message', chatbot_facade.additional_bot_message
  end

  def test_bot_message
    assert_equal 'Welcome message', chatbot_facade.bot_message
  end

  private

  def add_contact_value
    chatbot_facade.contact_value = '555'
  end
end
