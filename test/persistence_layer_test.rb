require 'test_helper'

class PersistenceLayerTest < Minitest::Test
  def test_find_or_create_chatbot_user
    chatbot = chatbot_facade.find_or_create_chatbot_user

    assert_equal 'Chatbot', chatbot.name
    assert_equal 'bot', chatbot.role
  end

  def test_create_bot_message
    build_bot_message
    message = chatbot_facade.create_bot_message

    assert_equal 'Welcome messageAditional message', message.body
  end

  def test_create_user_message
    add_user_message 'Hi'

    message = chatbot_facade.create_user_message

    assert_equal 'Hi', message.body
  end

  def test_create_user
    change_current_type_to :name
    add_user_message 'Wan'

    user = chatbot_facade.create_user

    assert_equal 'Wan', user.name
    assert_equal 'employee', user.role
  end

  private

  def build_bot_message
    chatbot_facade.build_bot_message
  end
end
