require 'test_helper'

class NextTypeBuilderTest < Minitest::Test
  def test_next_type
    assert_equal :name, chatbot_facade.next_type
  end

  def test_obligatory_next_type
    assert_equal :name, chatbot_facade.obligatory_next_type
  end

  def test_relative_next_type
    change_current_type_to :contact_approve
    change_current_bot_data

    assert_equal [{"1"=>:welcome}, {"2"=>:name}], chatbot_facade.relative_next_type
  end

  def test_determine_next_type
    change_current_type_to :contact_approve
    change_current_bot_data
    add_user_message '1'

    assert_equal :welcome, chatbot_facade.determine_next_type
  end
end
