module BotMessageStore
  PRIMARY_TYPE = :welcome
  MESSAGES = [
    {
      type: :welcome,
      message: 'I can help you with answers on all your related questions and help to find a great job!',
      obligatory_next_type: :name
    },
    {
      type: :name,
      message: 'Please enter your name',
      obligatory_next_type: :reach_out
    },
    {
      type: :reach_out,
      message: 'Hello NAME, How can we reach out to you?',
      aditional_message: "\n1: Phone, 2: Email, 3: No thanks",
      relative_next_type: [
        { '1' => :phone_contact_type },
        { '2' => :email_contact_type },
        { '3' => :contact_failed }
      ]
    },
    {
      type: :contact_failed,
      message: 'Said to hear that. Whenever you change your mind - feel free to send me a message',
      obligatory_next_type: :tragic_end
    },
    {
      type: :phone_contact_type,
      message: 'Please type your Phone Number',
      obligatory_next_type: :contact_time,
    },
    {
      type: :email_contact_type,
      message: 'Please type your Email Address',
      obligatory_next_type: :contact_approve
    },
    {
      type: :contact_time,
      message: 'What is the best time we can reach out of you?',
      obligatory_next_type: :contact_approve
    },
    {
      type: :contact_approve,
      message: 'We are going to contact you using CONTACT_VALUE',
      aditional_message: "\n1: Ok!, 2: Wrong email contact, 3: Wrong phone contact",
      relative_next_type: [
        {'1' => :happy_end},
        {'2' => :email_contact_type},
        {'3' => :phone_contact_type}
      ]
    }
  ]
end
