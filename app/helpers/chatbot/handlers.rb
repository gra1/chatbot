module Chatbot
  module Handlers
    def handle_bot_message
      print_bot_message
      create_bot_message
    end

    def handle_user_message
      build_user_message
      create_user_message
      create_user
    end
  end
end
