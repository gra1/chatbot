module Chatbot
  module BotMessageBuilder
    def print_bot_message
      puts build_bot_message
    end

    def build_bot_message
      additional_bot_message ? prepared_bot_message + additional_bot_message : prepared_bot_message
    end

    def prepared_bot_message
      case current_type
      when :reach_out
        bot_message.gsub('NAME', user_message)
      when :contact_time
        save_contact_value
        bot_message
      when :contact_approve
        bot_message.gsub('CONTACT_VALUE', contact_value || user_message)
      else
        bot_message
      end
    end

    def find_current_bot_data
      bot_messages.find { |i| i[:type].eql? current_type }
    end

    def additional_bot_message
      current_bot_data[:aditional_message]
    end

    def bot_message
      current_bot_data[:message]
    end
  end
end
