module Chatbot
  module PersistenceLayer
    def find_or_create_chatbot_user
      @_chatbot_user ||= User.find_or_create_by(role: 'bot', name: 'Chatbot')
    end

    def create_conversation
      bot_user.conversations.create
    end

    def create_bot_message
      Message.create(
        body: build_bot_message,
        conversation: conversation,
        user: bot_user
      )
    end

    def create_user_message
      Message.create(
        body: user_message,
        conversation: conversation
      )
    end

    def create_user
      if current_type.eql?(:name)
        User.create(
          name: user_message,
          role: 'employee'
        )
      end
    end
  end
end
