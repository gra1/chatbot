module Chatbot
  module NextTypeBuilder
    def next_type
      obligatory_next_type || determine_next_type
    end

    def determine_next_type
      relative_next_type.find { |i| i[user_message] }[user_message]
    end

    def obligatory_next_type
      current_bot_data[:obligatory_next_type]
    end

    def relative_next_type
      current_bot_data[:relative_next_type]
    end
  end
end
