require_relative 'models/user.rb'
require_relative 'models/message.rb'
require_relative 'models/conversation.rb'

require_relative 'helpers/chatbot/persistence_layer.rb'
require_relative 'helpers/chatbot/handlers.rb'
require_relative 'helpers/chatbot/bot_message_builder.rb'
require_relative 'helpers/chatbot/next_type_builder.rb'

class ChatbotFacade
  include Chatbot::PersistenceLayer
  include Chatbot::Handlers
  include Chatbot::BotMessageBuilder
  include Chatbot::NextTypeBuilder

  attr_accessor :current_type, :current_bot_data, :user_message, :conversation, :bot_user,
                :current_bot_message, :contact_value, :bot_messages

  def initialize(bot_messages, primary_type)
    @bot_messages = bot_messages
    @current_type = primary_type
    @current_bot_data = find_current_bot_data
    @bot_user = find_or_create_chatbot_user
    @conversation = create_conversation
  end

  def perform
    until end_types.include? current_type
      handle_bot_message
      handle_user_message
      @current_type = next_type
      @current_bot_data = find_current_bot_data
    end
  end

  private

  def save_contact_value
    @contact_value = user_message
  end

  def build_user_message
    @user_message = gets.chomp
  end

  def end_types
    @_end_types ||= [:happy_end, :tragic_end]
  end
end
